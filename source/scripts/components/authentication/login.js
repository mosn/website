import TemplateElement from '../TemplateElement.js'
import { Client } from '../../auth0-api.js'

class Login extends TemplateElement {
  constructor () {
    super()
    this.template = '/scripts/components/authentication/login.html'
  }

  async connectedCallback () {
    await super.connectedCallback()
    this.shadowRoot.addEventListener('submit', this._onSubmit)

    const preset =
      this.dataset.email ||
      localStorage.getItem('app.username') ||
      localStorage.getItem('app.email')

    if (preset) {
      const username = this.shadowRoot.querySelector('#username')
      username.value = preset
      username.removeAttribute('autofocus')
      this.shadowRoot.querySelector('#password')
        .setAttribute('autofocus', true)
    }
  }

  disconnectedCallback () {
    this.shadowRoot.removeEventListener('submit', this._onSubmit)
  }

  async _onSubmit (event) {
    event.preventDefault()

    this.querySelector('#progress').removeAttribute('hidden')
    this.querySelector('#problem').setAttribute('hidden', true)

    const username = event.target.querySelector('#username').value
    const password = event.target.querySelector('#password').value

    const auth0 = new Client()

    let accessToken
    try {
      const response = await auth0.getTokenByPassword(username, password)
      accessToken = response.access_token
    } catch (error) {
      this.querySelector('#problem output').textContent = error
      this.querySelector('#progress').setAttribute('hidden', true)
      this.querySelector('#problem').removeAttribute('hidden')
      return
    }

    localStorage.setItem('app.accessToken', accessToken)
    localStorage.setItem('app.username', username)
    localStorage.removeItem('app.email')

    window.history.pushState(null, '', '/sites')
    window.dispatchEvent(new PopStateEvent('popstate'))
  }
}

window.customElements.define('app-login', Login)
export default Login
